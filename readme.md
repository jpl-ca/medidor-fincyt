# Medidor - FINCyT #

El proyecto Medidor-FINCyT tiene como finalidad desarrollar un sistema de medición y registro de energía eléctrica y volumen de agua. El resultado del proyecto es un prototipo de dispositivo de medición y un sistema de software para gestionar los dispositivos medidores y visualizar sus mediciones.